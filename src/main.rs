// Compare the performance of vectors, stored on heap, and arrays, stored on stack.
// Implement a new data structure StackVec that can store short (fixed width) arrays
// on stack while longer vectors are stored as normal Vecs on heap.
// When INLINE_BUF_LEN (the capacity on stack) is larger or equal to BYTES array,
// the array is stored on stack, so setting INLINE_BUF_LEN=9 does the benchmark of
// array on stack and INLINE_BUF_LEN=8 performs the benchmark of the vector on heap.
// The benchmark tests both allocations (that should be significantly higher with vectors
// on the heap) and access and simple arithmetics witht the elements.

const INLINE_BUF_LEN:usize = 9;
const ADD_LOOPS:usize = 100;
const BYTES:[u8; 9] = [1,2,3,4,5,6,7,8,9];

#[derive(Debug, PartialEq)]
enum StackVec {
    Inline{
        inline: [u8; INLINE_BUF_LEN],
        length: usize,
    },
    Heap{heap: Vec<u8>},
}

impl StackVec {
    fn from_bytes(buf: &Vec<u8>) -> Self {
        if buf.len() <= INLINE_BUF_LEN {
            let mut indata = [0u8; INLINE_BUF_LEN];
            for i in 0..buf.len() {
                indata[i] = buf[i]
            }
            StackVec::Inline{
                inline:indata,
                length:buf.len()
            }
        } else {
            StackVec::Heap{heap:buf.clone()}
        }
    }

    fn add1(&mut self) { // Just do a lot of calculations with the elements.
        for _ in 0..ADD_LOOPS {
            match self {
                StackVec::Inline{inline:v, length: l} => {
                    for i in 0..(*l-1) {
                        let (r, _) = v[i].overflowing_add(v[i+1]);
                        v[i] = r;
                    }
                    let (r, _) = v[*l-1].overflowing_add(v[0]);
                    v[*l-1] = r;
                },
                StackVec::Heap{heap:v} => {
                    let l = v.len();
                    for i in 0..l-1 {
                        let (r, _) = v[i].overflowing_add(v[i+1]);
                        v[i] = r;
                    }
                    let (r, _) = v[l-1].overflowing_add(v[0]);
                    v[l-1] = r;
                },
            }
        }
    }
}

fn check_allocations(bytes: &Vec<u8>) -> StackVec {
    let mut sv = StackVec::from_bytes(bytes);
    sv.add1();
    sv
}

fn main() {
    let bytes = Vec::from(BYTES);
    let tst_vector:[u8; 9] = [160, 93, 164, 180, 84, 108, 60, 228, 156];

    let sv = check_allocations(&bytes); // Verify the allocation and manipulation of the vector works.
    assert_eq!(sv, StackVec::from_bytes(&(tst_vector.to_vec())));

    for _ in 1..1_000_000 {
        check_allocations(&bytes);
    }
}
